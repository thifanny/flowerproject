<?php

error_reporting(E_ALL);
ini_set('display_errors', 1);

require_once $_SERVER['DOCUMENT_ROOT'] .'/config/Database.php';

class Order {

  public $db;

  public function __construct()
  {
    $pdo = new Database();
    $this->db = $pdo->getDb();
  }

  /**
  * Récupère toutes les commandes de la db
  * @method findAll
  * @return array  [résultats de  la requete sql]
  */
  public function findAll() : array
  {

    $req = $this->db->prepare('SELECT * FROM bill ORDER BY created_at DESC');
    $req->execute();

    /* Récupération de toutes les lignes d'un jeu de résultats */
    $results = $req->fetchAll(PDO::FETCH_ASSOC);

    return $results;
  }

  /**
  * Récupère toutes les commandes d'un utilisateur de la db
  * @method findByUser
  * @param  integer $id
  * @return array
  */
  public function findByUser($id) : array
  {
    $query =
    'SELECT bill.id, bill.total, bill.created_at, bill.status
    FROM bill
    WHERE user_id = :user_id
    ORDER BY status DESC';

    $req = $this->db->prepare($query);
    $req->execute(array(
        'user_id' => $id
      ));

    /* Récupération de toutes les lignes d'un jeu de résultats */
    $results = $req->fetchAll(PDO::FETCH_ASSOC);

    return $results;
  }

 /**
  * Récupération des détails d'une commande
  * @method findBillDetails
  * @param  integer $id [description]
  * @return array [description]
  */
  public function findBillDetails($id) : array
  {
    $query =
    'SELECT bill_detail.product_id, bill_detail.product_qty, bill_detail.subtotal,
    product.name, product.price
    FROM bill_detail
    INNER JOIN bill
    ON bill_detail.bill_id = bill.id
    RIGHT JOIN product
    ON bill_detail.product_id = product.id
    WHERE bill.id = :bill_id';

    $req = $this->db->prepare($query);
    $req->execute(array(
        'bill_id' => $id
      ));

    /* Récupération de toutes les lignes d'un jeu de résultats */
    $results = $req->fetchAll(PDO::FETCH_ASSOC);

    return $results;
  }

  /**
   * Ajout d'une commande à la db
   * @method addBill
   * @param  array $datas [description]
   * @return int [description]
   */
  public function addBill(array $datas) : int
  {
    $query =
    'INSERT INTO bill (user_id, created_at, total, status)
    VALUES (:user_id, :created_at, :total, :status)';

    $req = $this->db->prepare($query);
    $req->execute(array(
      'user_id'     => $datas['user_id'],
      'created_at'  => $datas['created_at'],
      'total'       => $datas['total'],
      'status'      => $datas['status'],
    ));

    $result = $req->fetch(PDO::FETCH_ASSOC);

    $id = $this->db->lastInsertId();
    return $id;
  }

 /**
  * Ajout des détails d'une commande à la db
  * @method addBillDetails
  * @param  array $datas [description]
  * @return int $id
  */
  public function addBillDetails(array $datas) : int
  {

    $query =
    'INSERT INTO bill_detail (bill_id, product_id, product_qty, subtotal)
    VALUES (:bill_id, :product_id, :product_qty, :subtotal)';

    $req = $this->db->prepare($query);
    $req->execute(array(
      'bill_id'     => $datas['bill_id'],
      'product_id'  => $datas['product_id'],
      'product_qty' => $datas['product_qty'],
      'subtotal'    => $datas['subtotal'],
    ));

    $result = $req->fetch(PDO::FETCH_ASSOC);

    $id = $this->db->lastInsertId();

    return $id;
  }

  /**
  * Modifie le total à la commande
  * @method updateTotal
  * @param  array $datas [description]
  * @return bool $result
  */
  public function updateTotal(array $datas) : bool
  {
    $query =
      "UPDATE bill SET total = :total WHERE id = :id";

    $req = $this->db->prepare($query);

    $result = $req->execute(array(
      'total' => $datas['total'],
      'id' => $datas['id']
    ));

    return $result;
  }

  /**
   * Met à jour le statut une commande de la db
   * @method updateStatus
   * @param  array $datas [description]
   * @return bool $result
   */
    public function updateStatus(array $datas) : bool
  {
    $query =
      "UPDATE bill SET status = :status WHERE id = :id";

    $req = $this->db->prepare($query);

    $result = $req->execute(array(
      'status' => $datas['status'],
      'id' => $datas['id']
    ));

    return $result;
  }

}
