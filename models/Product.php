<?php

error_reporting(E_ALL);
ini_set('display_errors', 1);

require_once $_SERVER['DOCUMENT_ROOT'] . '/config/Database.php';

class Product {

  public $db;

  public function __construct() {
    $pdo = new Database();
    $this->db = $pdo->getDb();
  }

  /**
   * Récupère tous les produits de la db
   * @method findAll
   * @return array [description]
   */
  public function findAll() : array
  {

    $req = $this->db->prepare('SELECT * FROM product WHERE display = :display');
    $req->execute(array(
      'display' => 1
    ));
    /* Récupération de toutes les lignes d'un jeu de résultats */
    $results = $req->fetchAll(PDO::FETCH_ASSOC);

    return $results;
  }

  /**
   * Récupère les 3 produits les + récents de la db
   * @method findNews
   * @return array [description]
   */
  public function findNews() : array
  {

    $req = $this->db->prepare('SELECT * FROM product WHERE display = :display ORDER BY created_at DESC LIMIT 3');
    $req->execute(array(
      'display' => 1
    ));
    /* Récupération de toutes les lignes d'un jeu de résultats */
    $results = $req->fetchAll(PDO::FETCH_ASSOC);

    return $results;
  }

  /**
   * Récupère un produit de la db par son id
   * @method findOne
   * @param  integer $id [description]
   * @return array [description]
   */
  public function findOne(int $id = null) : array
  {

    $req = $this->db->prepare('SELECT * FROM product WHERE id = :id AND display = :display');
    $req->execute(array(
      'id'      => $id,
      'display' => 1
    ));

    $result = $req->fetch(PDO::FETCH_ASSOC);
    return $result;
  }

  /**
   * Ajoute un produit à la db
   * @method add
   * @param  array $datas [description]
   * @return bool [description]
   */
  public function add(array $datas) : bool
  {
    $query =
    "INSERT INTO product (name, reference, price, quantity, path_img, description, created_at, display)
    VALUES (:name, :reference, :price, :quantity, :path_img, :description, :created_at, :display)";

    $req = $this->db->prepare($query);
    $result = $req->execute(array(
      'name'        => $datas['name'],
      'reference'   => $datas['reference'],
      'price'       => $datas['price'],
      'quantity'    => $datas['quantity'],
      'path_img'    => $datas['path_img'],
      'description' => $datas['description'],
      'created_at'  => $datas['created_at'],
      'display'     => 1
    ));

    return $result;
  }

  /**
   * Met à jour un produit de la db
   * @method update
   * @param  array $datas [description]
   * @return bool [description]
   */
  public function update(array $datas) : bool
  {
    $query =
      "UPDATE product
      SET name = :name,
      reference = :reference,
      price = :price,
      quantity = :quantity,
      path_img = :path_img,
      description = :description
      WHERE id = :id";

    $req = $this->db->prepare($query);
    $result = $req->execute(array(
      'name'        => $datas['name'],
      'reference'   => $datas['reference'],
      'price'       => $datas['price'],
      'quantity'    => $datas['quantity'],
      'path_img'    => $datas['path_img'],
      'description' => $datas['description'],
      'id'          => $datas['id'],
    ));

    return $result;
  }

  /**
   * Enlève un produit du site
   * @method displayOff
   * @param  integer $id [description]
   * @return bool [description]
   */
  public function displayOff(int $id) : bool
  {
    $query =
      "UPDATE product
      SET display = :display
      WHERE id = :id";

    $req = $this->db->prepare($query);
    $result = $req->execute(array(
      'display' => 0,
      'id'      => $id
    ));

    return $result;
  }

}
