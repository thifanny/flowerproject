<?php

error_reporting(E_ALL);
ini_set('display_errors', 1);

require_once $_SERVER['DOCUMENT_ROOT'] .'/config/Database.php';

class User {

  public $db;

  public function __construct()
  {
    $pdo = new Database();
    $this->db = $pdo->getDb();
  }

  /*
  * Récupère tous les users de la db
  */
  public function findAll()
  {

    $req = $this->db->prepare('SELECT * FROM user');
    $req->execute();
    /* Récupération de toutes les lignes d'un jeu de résultats */
    $results = $req->fetchAll(PDO::FETCH_ASSOC);
    // var_dump($results);

    return $results;
  }

  /*
  * Récupère un user de la db par son id
  */
  public function findUserById($id = null)
  {

    $req = $this->db->prepare('SELECT * FROM user WHERE id = :id');
    $req->execute(array(
      'id' => $id
    ));

    $result = $req->fetch(PDO::FETCH_ASSOC);
    return $result;
  }

  /*
  * Récupère un user de la db par son email
  */
  public function findUserByEmail($email)
  {

    $req = $this->db->prepare('SELECT * FROM user WHERE email = :email');
    $req->execute(array(
      'email' => $email
    ));

    $result = $req->fetch(PDO::FETCH_ASSOC);
    return $result;
  }

  /*
  * Ajoute d'un user à la db
  */
  public function addAccount($datas)
  {
    // INSERT INTO table (nom_colonne_1, nom_colonne_2, ...
    // VALUES ('valeur 1', 'valeur 2', ...)

    $query = 'INSERT INTO user (email, password, status, address, city, postal_code, created_at,  first_name, last_name)
    VALUES (:email, :password, :status, :address, :city, :postal_code, :created_at, :first_name, :last_name)';

    $req = $this->db->prepare($query);
    $req->execute(array(
      'email'       => $datas['email'],
      'password'    => $datas['password'],
      'status'      => $datas['status'],
      'address'     => $datas['address'],
      'city'        => $datas['city'],
      'postal_code' => $datas['postal_code'],
      'created_at'  => $datas['created_at'],
      'first_name'  => $datas['first_name'],
      'last_name'   => $datas['last_name']
    ));

    $result = $req->fetch(PDO::FETCH_ASSOC);

    $id = $this->db->lastInsertId();

    return $id;
  }

  /*
  * Met à jour un user de la db
  */
  public function updateInfo($datas){
    $query =
      "UPDATE user
      SET first_name = :first_name,
      last_name = :last_name,
      email = :email,
      address = :address,
      city = :city,
      postal_code = :postal_code
      WHERE id = :id";

    $req = $this->db->prepare($query);
    $result = $req->execute(array(
      'first_name' => $datas['firstName'],
      'last_name' => $datas['lastName'],
      'email' => $datas['email'],
      'address' => $datas['address'],
      'city' => $datas['city'],
      'postal_code' => $datas['postal_code'],
      'id' => $datas['id'],
    ));

    return $result;
  }

  /*Met à jour le mot de passe*/
  public function updatePwd($datas){
    $query =
      "UPDATE user
      SET password = :password
      WHERE id = :id";

    $req = $this->db->prepare($query);
    $result = $req->execute(array(
      'password' => $datas['password'],
      'id' => $datas['id']
    ));

    return $result;
  }

  /*
  * Supprime un user de la db
  */
  public function delete($id)
  {
    $query = 'DELETE FROM user WHERE id = :id';

    $req = $this->db->prepare($query);

    $result = $req->execute(array(
      'id' => $id
    ));

    return $result;
  }

}
