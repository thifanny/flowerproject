Site de commerce de fleuriste

----------------------------

Sans authentification :

  - Page d'accueil : présentation de la boutique, présentation des trois derniers articles
  - Page Produits : affichage des différents produits avec leur résumé -> possibilité d'ajouter au panier
  - Page produit : description entière de l'article -> possibilité d'ajouter au panier
  - Page Panier : Si articles, liste des articles : la quantité, le prix. Possibilité de supprimer ou ajouter en qté les articles
  - Page de connexion/enregistrement : Possibilité de créer un compte ou de s'y connecter. Vérification JS du format, vérification serveur.

-----------------------------

Connection utilisateur :

  - Accueil et produits (identique à la non-authentification)
  - Compte : récapitulatif des informations du compte : possibilité de modifier les informations, le mot de passe ou de supprimer le compte
  - Commandes : récapitulatif des commandes passées ainsi que leur statut.
  - Panier : récupère le panier si commencé avant connexion. Même système que lors de non auth. Possibilité de passer commande, possibilité de modification des informations de compte.
  - Page de déconnexion (et destruction de la session)
  
------------------------------

Connection admin :

  - Page de commande : récapitulatif des commandes passées par les utilisateurs. Possibilité de modifier le statut de la commande. Possibilité de connaitre l'utilisateur qui a passé la commande et son adresse, le détail de la commande
  - Page de produits : récapitulatif des articles disponibles sur le site. Possibilité de modifier, ajouter ou supprimer(rendre indisponible) des articles.
  - Page client : récapitulatif des clients qui ont un compte. En lecture seule.
  - Page de déconnexion (et destruction de la session)
