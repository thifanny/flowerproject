import { displayInfoForm, displayPwdForm, deleteAccount } from './modules/account.js';
import { test } from './modules/utilities/utilities.js';
import { addToCart, displayCart, postOrder } from './modules/cart.js';
import { checkPwd, checkEmail, checkWord, checkAddress, checkPostal, getInfo } from './modules/auth.js';

////////////////////////////////////////////////////////////////
                        // SHOPPING CART //
////////////////////////////////////////////////////////////////

// Evenement quand on ajoute un élément au panier
let btnsAddCart = document.getElementsByClassName('btnCart');
for (let i = 0; i < btnsAddCart.length ; i++) {
  let btnAddCart = btnsAddCart[i];
  btnAddCart.addEventListener('click', function(e){
   addToCart(btnAddCart) ;
  });
}

// // Affichage du panier
if(document.getElementById('cart')){

  let btnCart = document.getElementById('cart');
  btnCart.addEventListener('click', function(e){
    displayCart();
    window.location == 'http://localhost:8000/controllers/CartCtrl.php';
  })
}

if(document.getElementById('btnOrder')){
  let btnOrder = document.getElementById('btnOrder');
  btnOrder.addEventListener('click', function(e){
    postOrder();
    window.location == 'http://localhost:8000/controllers/PostOrderCtrl.php';
  })
}

////////////////////////////////////////////////////////////////
                        // REGISTER && LOGIN
////////////////////////////////////////////////////////////////

if(document.getElementById('sectionRegister') || document.getElementById('sectionLogin'))
{

  let form = document.getElementsByTagName('form')[0];
  let pwdInput = document.getElementById('password');
  let emailInput = document.getElementById('email');
  let btnSubmit = document.getElementById('btnSubmit');

  // Vérification du mot de passe lors de la perte de focus
  pwdInput.addEventListener('blur', function(){
    let bool = checkPwd(pwdInput.value);
    let info = document.getElementById('infoPwd');
    getInfo(bool, info);
  });

  // Vérification de l'email lors de la perte de focus
  emailInput.addEventListener('blur', function(){
    let bool = checkEmail(emailInput.value);
    let info = document.getElementById('infoEmail');
    getInfo(bool, info);
  });

  if(document.getElementById('sectionLogin')){
    form.addEventListener('change', function(){
      if(checkEmail(emailInput.value) && checkPwd(pwdInput.value)){
        btnSubmit.removeAttribute('disabled');
        btnSubmit.classList.remove('disabled');
      } else {
        btnSubmit.setAttribute('disabled', 'true');
        btnSubmit.classList.add('disabled');
      }
    })
  }

  if(document.getElementById('sectionRegister')){
    let firstNameInput = document.getElementById('first_name');
    let lastNameInput = document.getElementById('last_name');
    let addressInput = document.getElementById('address');
    let postalInput = document.getElementById('postal_code');
    let cityInput = document.getElementById('city');

    // Vérification du prénom lors de la perte de focus
    firstNameInput.addEventListener('blur', function(){
      let bool = checkWord(firstNameInput.value);
      let info = document.getElementById('infoFirstName');
      getInfo(bool, info);
    })

    // Vérification du nom lors de la perte de focus
    lastNameInput.addEventListener('blur', function(){
      let bool = checkWord(lastNameInput.value);
      let info = document.getElementById('infoLastName');
      getInfo(bool, info);
    })

    // Vérification de l'adresse lors de la perte de focus
    addressInput.addEventListener('blur', function(){
      let bool = checkAddress(addressInput.value);
      let info = document.getElementById('infoAddress');
      getInfo(bool, info);
    })

    // Vérification du code postal lors de la perte de focus
    postalInput.addEventListener('blur', function(){
      let bool = checkPostal(postalInput.value);
      let info = document.getElementById('infoPostal');
      getInfo(bool, info);
    })

    // Vérification de la ville lors de la perte de focus
    cityInput.addEventListener('blur', function(){
      let bool = checkWord(cityInput.value);
      let info = document.getElementById('infoCity');
      getInfo(bool, info);
    })

    form.addEventListener('change', function(){
      if(checkEmail(emailInput.value)
        && checkPwd(pwdInput.value)
        && checkWord(firstNameInput.value)
        && checkWord(lastNameInput.value)
        && checkAddress(addressInput.value)
        && checkPostal(postalInput.value)
        && checkWord(cityInput.value))
      {
        btnSubmit.removeAttribute('disabled');
        btnSubmit.classList.remove('disabled');
      } else {
        btnSubmit.setAttribute('disabled', 'true');
        btnSubmit.classList.add('disabled');
      }
    })
  }

}

////////////////////////////////////////////////////////////////
                        // USER ACCOUNT //
////////////////////////////////////////////////////////////////

if(document.getElementById('sectionAccount')){

  let updateInfoBtn = document.getElementById('updateInfoBtn');
  let updatePwdBtn = document.getElementById('updatePwdBtn');
  let deleteAccountBtn = document.getElementById('deleteAccountBtn');

  updateInfoBtn.addEventListener('click', displayInfoForm);
  updatePwdBtn.addEventListener('click', displayPwdForm);
  deleteAccountBtn.addEventListener('click', function(e){
    deleteAccount(e);
  });

  if(document.getElementById('sectionUpdateAccount')){

    let form = document.getElementById('infoForm');
    let firstNameInput = document.getElementById('first_name');
    let lastNameInput = document.getElementById('last_name');
    let emailInput = document.getElementById('email');
    let addressInput = document.getElementById('address');
    let postalInput = document.getElementById('postal_code');
    let cityInput = document.getElementById('city');
    let btnSubmit = document.getElementById('btnPostInfo');

    // Vérification du prénom lors de la perte de focus
    firstNameInput.addEventListener('blur', function(){
      let bool = checkWord(firstNameInput.value);
      let info = document.getElementById('infoFirstName');
      getInfo(bool, info);
    })

    // Vérification du nom lors de la perte de focus
    lastNameInput.addEventListener('blur', function(){
      let bool = checkWord(lastNameInput.value);
      let info = document.getElementById('infoLastName');
      getInfo(bool, info);
    })

    // Vérification de l'email lors de la perte de focus
    emailInput.addEventListener('blur', function(){
      let bool = checkEmail(emailInput.value);
      let info = document.getElementById('infoEmail');
      getInfo(bool, info);
    });

    // Vérification de l'adresse lors de la perte de focus
    addressInput.addEventListener('blur', function(){
      let bool = checkAddress(addressInput.value);
      let info = document.getElementById('infoAddress');
      getInfo(bool, info);
    })

    // Vérification du code postal lors de la perte de focus
    postalInput.addEventListener('blur', function(){
      let bool = checkPostal(postalInput.value);
      let info = document.getElementById('infoPostal');
      getInfo(bool, info);
    })

    // Vérification de la ville lors de la perte de focus
    cityInput.addEventListener('blur', function(){
      let bool = checkWord(cityInput.value);
      let info = document.getElementById('infoCity');
      getInfo(bool, info);
    })

    form.addEventListener('change', function(){
      if(checkEmail(emailInput.value)
        && checkWord(firstNameInput.value)
        && checkWord(lastNameInput.value)
        && checkAddress(addressInput.value)
        && checkPostal(postalInput.value)
        && checkWord(cityInput.value))
      {
        btnSubmit.removeAttribute('disabled');
        btnSubmit.classList.remove('disabled');
      } else {
        btnSubmit.setAttribute('disabled', 'true');
        btnSubmit.classList.add('disabled');
      }
    })

  }

  if(document.getElementById('sectionUpdatePwd')){

    let formPwd = document.getElementById('pwdForm');
    let oldPwd = document.getElementById('oldPwd');
    let newPwd = document.getElementById('newPwd');
    let btnSubmitPwd = document.getElementById('btnPostPwd');

    // Vérification de l'ancien mot de passe lors de la perte de focus
    oldPwd.addEventListener('blur', function(){
      let bool = checkPwd(oldPwd.value);
      let info = document.getElementById('infoOldPwd');
      getInfo(bool, info);
    })

    // Vérification du nouveau mdp lors de la perte de focus
    newPwd.addEventListener('blur', function(){
      let bool = checkPwd(newPwd.value);
      let info = document.getElementById('infoNewPwd');
      getInfo(bool, info);
    })

    formPwd.addEventListener('change', function(){
      if(checkPwd(newPwd.value) && checkPwd(oldPwd.value)){
        btnSubmitPwd.removeAttribute('disabled');
        btnSubmitPwd.classList.remove('disabled');
      } else {
        btnSubmitPwd.setAttribute('disabled', 'true');
        btnSubmitPwd.classList.add('disabled');
      }
    })

  }
}

////////////////////////////////////////////////////////////////
                  // Admin Order //
////////////////////////////////////////////////////////////////

if(document.getElementById('sectionAdminOrder')){

  let btnsOrder = document.getElementsByClassName('btnsOrder');
  let details = document.getElementsByClassName('details');

  for (let i = 0; i < btnsOrder.length; i++) {

    btnsOrder[i].addEventListener('click', function(e){
      if(details[i].classList.contains('hidden')){
        details[i].classList.remove('hidden');
      } else {
        details[i].classList.add('hidden');
        console.log(details[i]);
      }
    })

  }

}

////////////////////////////////////////////////////////////////
                  // Apparence //
////////////////////////////////////////////////////////////////
let burger = document.getElementById('burgerNav');
let nav = document.getElementById('nav');

nav.classList.remove('show');
nav.classList.add('hidden');

burger.addEventListener('click', function(){
  if(nav.classList.contains('show')){
    nav.classList.remove('show');
    nav.classList.add('hidden');
  } else if(nav.classList.contains('hidden')){
    burger.classList.remove('pinkBg');
    nav.classList.remove('hidden');
    nav.classList.add('show');
  }

  let choices = nav.getElementsByTagName('li');

  for(let i = 0; i < choices.length; i++){
    choices[i].addEventListener('mouseup', function(){
      nav.classList.remove('show');
      nav.classList.add('hidden');
    })
  }
})
