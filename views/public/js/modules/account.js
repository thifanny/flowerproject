export { displayInfoForm, displayPwdForm, deleteAccount };

function displayInfoForm(){

  let sectionUpdateAccount = document.getElementById('sectionUpdateAccount');
  let btnUndoInfo = document.getElementById('btnUndoInfo');

  if(sectionUpdateAccount.classList.contains('hidden')){
    sectionUpdateAccount.classList.remove('hidden');
  }

  btnUndoInfo.addEventListener('click', function(e){
    e.preventDefault();
    sectionUpdateAccount.classList.add('hidden');
  })

}

function displayPwdForm(){
  let sectionUpdatePwd = document.getElementById('sectionUpdatePwd');
  let btnUndoPwd = document.getElementById('btnUndoPwd');

  if(sectionUpdatePwd.classList.contains('hidden')){
    sectionUpdatePwd.classList.remove('hidden');
  }

  btnUndoPwd.addEventListener('click', function(e){
    e.preventDefault();
    sectionUpdatePwd.classList.add('hidden');
  })

}

function deleteAccount(e){
  e.preventDefault();
  let confirmSentence = 'Votre compte et toutes vos informations seront définitivement supprimés. Etes-vous sûr de vouloir supprimer votre compte ?'
  if(confirm(confirmSentence)){
    let deleteForm = document.getElementById('deleteForm');
    deleteForm.submit();
  }
}
