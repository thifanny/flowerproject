// Imports
import { KEY, getCart, saveCart, removeCart } from './utilities/localStorage.js';
import { ajaxPost } from './utilities/ajax.js';

// Exports
export { addToCart, displayCart, postOrder }

//Création dynamique du formulaire
function createForm(url, params){
  var form = document.createElement('form');
  form.setAttribute('method', 'POST');
  form.setAttribute('action', url);

  var champCache = document.createElement('input');
  champCache.setAttribute('type', 'hidden');
  champCache.setAttribute('name', KEY);
  champCache.setAttribute('value', params);
  form.appendChild(champCache);
  document.body.appendChild(form);

  return form;

}

// Affiche le panier
function displayCart() {

  if(getCart(KEY) != null){
    var cart = JSON.stringify(getCart(KEY));
  }

  let postForm = createForm('/controllers/CartCtrl.php', cart);
  let result = postForm.submit();

}

// Ajoute un élément au panier
function addToCart(btnCart){
  let tab = [];

  if(getCart(KEY) != null){
    tab = getCart(KEY);
  }

  let btnCartId = parseInt(btnCart.id);
  tab.push(btnCartId);
  let result = saveCart(KEY, tab);
  alert("L'article a bien été ajouté au panier")
  return true;
}

// Enlève un élément du panier
function removeToCart(btnCart) {
  let tab = [];
  let btnCartId = parseInt(btnCart.id);

  if(getCart(KEY) != null){
    tab = getCart(KEY);
  }

  let index = tab.indexOf(btnCartId);
  if (index !== -1) {
    tab.splice(index, 1);
  }

  let result = saveCart(KEY, tab);
  return true;
}

let article = document.getElementsByClassName('articleProduct');

let btnLessArticle = document.getElementsByClassName('lessArticle');
let btnMoreArticle = document.getElementsByClassName('moreArticle');

for(var i= 0; i < article.length; i++) {

  let qtyArticle = document.getElementsByClassName('qtyArticle')[i];
  let qty = parseInt(qtyArticle.innerHTML);

  btnLessArticle[i].addEventListener('click', function(e){
    console.log(qty);
    qty -= 1;
    qtyArticle.innerHTML = qty;

    removeToCart(this);
    displayCart();
  })

  btnMoreArticle[i].addEventListener('click', function(e){

    qty += 1;
    qtyArticle.innerHTML = qty;

    addToCart(this);
    displayCart();
  })


}

function postOrder(){

  if(getCart(KEY) != null){
    var cart = JSON.stringify(getCart(KEY));
  }

  let postForm = createForm('/controllers/user/PostOrderCtrl.php', cart);
  let result = postForm.submit();

  removeCart();
}
