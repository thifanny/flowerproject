export { KEY, getCart, saveCart, removeCart}

const KEY = 'cart';

function getCart(key){

  let values = localStorage.getItem(KEY);

  if(values) {
    let datas = JSON.parse(values);
    return datas;
  } else {
    return null;
  }

}

function saveCart(key, datas){
  localStorage.setItem(key, JSON.stringify(datas));
  // return true;
}

function removeCart(key){
  localStorage.clear();
}
