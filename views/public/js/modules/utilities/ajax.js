import { KEY, getCart, saveCart } from './localStorage.js';

export { ajaxPost }

function ajaxPost(url, datas){
  var xhr = new XMLHttpRequest();

  xhr.open("POST", url);
  xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
  xhr.addEventListener('readystatechange', function() { // On gère ici une requête asynchrone

      if (xhr.readyState === XMLHttpRequest.DONE && xhr.status === 200) { // Si le fichier est chargé sans erreur
          document.getElementsByTagName('body')[0].innerHTML = xhr.responseText;
      } else if (xhr.readyState === XMLHttpRequest.DONE && xhr.status != 200) { // En cas d'erreur !
          alert('Une erreur est survenue !\n\nCode :' + xhr.status + '\nTexte : ' + xhr.statusText);
      }
  });
  // xhr.send(cart);
  xhr.send(datas);
}

// function ajaxGet(url){
//   var xhr = new XMLHttpRequest();
//   xhr.open('GET', url);
//   xhr.send(null);
//
//   xhr.addEventListener('readystatechange', function() { // On gère ici une requête asynchrone
//
//     if (xhr.readyState === XMLHttpRequest.DONE && xhr.status === 200) { // Si le fichier est chargé sans erreur
//
//         document.getElementById('fileContent').innerHTML = '<span>' + xhr.responseText + '</span>'; // On l'affiche !
//
//     } else if (xhr.readyState === XMLHttpRequest.DONE && xhr.status != 200) { // En cas d'erreur !
//
//         alert('Une erreur est survenue !\n\nCode :' + xhr.status + '\nTexte : ' + xhr.statusText);
//
//     }
//
// });
// }
