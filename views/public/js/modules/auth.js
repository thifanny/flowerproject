export { checkPwd, checkEmail, checkWord, checkAddress, checkPostal, getInfo } ;

// Vérifie le format du mot de passe : au moins 8 caractères dont une majuscule, une minuscule, un chiffre et un caractère spécial
function checkPwd(pwd){
  let regexPwd = /^(?=.*[A-Z])(?=.*[.!@#$&*])(?=.*[0-9])(?=.*[a-z]).{8,}$/;
  if (!pwd.match(regexPwd)){
    return false;
  } else {
    return true;
  }
}

// Vérifie le format de l'email
function checkEmail(email){
  let regexEmail = /^[_a-z0-9-]+(\.[_a-z0-9-]+)*(\+[a-z0-9-]+)?@[a-zA-Z0-9_]+(\.[a-zA-Z0-9_]+)*\.[a-zA-Z]{2,4}$/;
  if (!email.match(regexEmail)){
    return false;
  } else {
    return true;
  }
}

// Vérifie les noms (accepte les accents et le trait d'union)
function checkWord(word){
  let regexWord = /^[-a-zA-Zéèêàâëïîäôöùü]{3,}$/;
  if (!word.match(regexWord)){
    return false;
  } else {
    return true;
  }
}

// Vérifie l'adresse (accepte les accents, les traits d'union, les espaces et les chiffres)
function checkAddress(address){
  let regexAddress = /^[-a-zA-Z0-9éèêàâëïîäôöùü\s]{3,}$/;
  if (!address.match(regexAddress)){
    return false;
  } else {
    return true;
  }
}

// Vérifie que le code postal contienne 5 chiffres
function checkPostal(postal){
  let regexPostal = /^[0-9]{5}$/;
  if (!postal.match(regexPostal)){
    return false;
  } else {
    return true;
  }
}

// Affiche les informations pour l'utilisateur
function getInfo(bool, info) {
  if (!bool){
    info.classList.remove('hidden');
  } else {
    info.classList.add('hidden');
  }
}
