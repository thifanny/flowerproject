<?php

error_reporting(E_ALL);
ini_set('display_errors', 1);

class Checker {

  public function __construct(){}

  /**
   * Vérification des noms (avec accent, et trait d'union)
   * @method checkWord
   * @param  string $content
   * @return bool $result
   */
  public function checkWord(string $content) : bool
  {
    $contentCheck = trim(htmlentities($content));
    $regex = '/^[-a-zA-Zéèêàâëïîäôöùü]{3,}$/';
    $result = preg_match($regex, $content);
    return $result;

  }

  /**
   * Vérification du code postal : seulement 5 chiffres
   * @method checkPostalCode
   * @param  int $content
   * @return bool $result
   */
  public function checkPostalCode(int $content) : bool
  {
    $contentCheck = trim(htmlentities($content));

    if(strlen($contentCheck) == 5 && ctype_digit($contentCheck) ){
      return $result = true ;
    } else {
      return $result = false;
    }

  }

  /**
   * Vérification du format d'adresse
   * @method checkAddress
   * @param  string $content [description]
   * @return bool [description]
   */
  public function checkAddress(string $content) :bool
  {
    $content = htmlentities($content);
    $regex = '/^[-a-zA-Z0-9éèêàâëïîäôöùü\s]{3,}$/';
    $result = preg_match($regex, $content);
    return $result;
  }

  /**
   * Vérification du format de mail
   * @method checkEmailFormat
   * @param  string $content
   * @return bool $result
   */
  public function checkEmailFormat(string $content) : bool
  {
    $content = htmlentities($content);
    $regex = '/^[_a-z0-9-]+(\.[_a-z0-9-]+)*(\+[a-z0-9-]+)?@[a-zA-Z0-9_]+(\.[a-zA-Z0-9_]+)*\.[a-zA-Z]{2,4}$/';
    $result = preg_match($regex, $content);
    return $result;
  }

  /**
   * Vérification du format de mot de passe : au moins 8 caractères dont une majuscule, une minuscule, un chiffre et un caractère spécial
   * @method checkPasswordFormat
   * @param  string $content
   * @return bool $result
   */
  public function checkPasswordFormat(string $content) :bool
  {
    $content = htmlentities($content);
    $regex = '/^(?=.*[A-Z])(?=.*[.!@#$&*])(?=.*[0-9])(?=.*[a-z]).{8,}$/';
    $result = preg_match($regex, $content);
    return $result;
  }
}
