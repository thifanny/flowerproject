<?php

class Format {

  /**
  * Applique un format 'd/m/Y' à une date
  * @method setFormatDate
  * @param  string $strDate
  * @return $date
  */
  public function setFormatDate(string $strDate)
  {
    $date = new DateTime($strDate);
    return $date->format('d/m/Y');
  }

  /**
   * Transformation la date (chaine de caractère en objet DateTime() )
   * @method setDeliveryDate
   * @param string $orderDate
   * @return $deliveryDate
   */
  public function setDeliveryDate($orderDate)
  {
    $date = new DateTime($orderDate);
    $deliveryDate = $date->add(new DateInterval('P07D'));
    return $deliveryDate->format('d/m/Y');
  }

  /**
   * Donne le statut selon sa référence numérique
   * @method setStatus
   * @param integer $status
   * @return string
   */
  private function setStatus($status)
  {
    switch($status){
      case 1 :
        return 'Envoyé';
        break;
      case 2 :
        return 'En cours de préparation';
        break;
      case 3 :
        return 'En attente';
        break;
      default :
        return 'Statut inconnu';
        break;
    }
  }
}
