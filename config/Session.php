<?php

class Session
{
  protected $userId;
  protected $userStatus;

  public function __construct()
  {
    if(session_status() === PHP_SESSION_NONE) {
      session_start();
    }

    if(isset($_SESSION['id']) && !empty($_SESSION['id'])){
      $this->userId = intval($_SESSION['id']);
      $this->userStatus = intval($_SESSION['status']);
    } else {
      $this->userId = null;
      $this->userStatus = null;
    }

  }

  /**
   * Détruiy la session
   * @method destroy
   */
  public function destroy()
  {
    // Détruit toutes les variables de session
    $_SESSION = array();

    // Si vous voulez détruire complètement la session, effacez également
    // le cookie de session.
    // Note : cela détruira la session et pas seulement les données de session !
    if (ini_get("session.use_cookies")) {
        $params = session_get_cookie_params();
        setcookie(session_name(), '', time() - 42000,
            $params["path"], $params["domain"],
            $params["secure"], $params["httponly"]
        );
    }

    // Finalement, on détruit la session.
    session_destroy();
  }

}
