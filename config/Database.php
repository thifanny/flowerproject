<?php

class Database
{
  private $db;
  private $host;
  private $port;
  private $dbname;
  private $username;
  private $password;

  public function __construct()
  {
    $this->setDb();
  }

  /**
   * Retourne la base de données
   * @method getDb
   * @return object $db
   */
  public function getDb()
  {
    try {
      $dsn = 'mysql:host=' . $this->host . ';port=' . $this->port . ';dbname=' . $this->dbname . ";charset=utf8" ;
      $this->db = new PDO($dsn, $this->username, $this->password);
      return $this->db;
    }
    catch (PDOException $e) {
      echo 'Connexion échouée : ' . $e->getMessage();
    }
  }

  private function setDb()
  {
    try {
      $json_source = file_get_contents($_SERVER['DOCUMENT_ROOT'] ."/config/config.json");
      $json_data = json_decode($json_source);

      $this->host = $json_data->host;
      $this->port = $json_data->port;
      $this->dbname = $json_data->dbname;

      $this->username = $json_data->username;
      $this->password = $json_data->password;
    } catch (Exception $e){
      $e = 'Problème de connexion db';
    }
  }

}
