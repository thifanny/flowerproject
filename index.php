<?php

error_reporting(E_ALL);
ini_set('display_errors', 1);

require_once $_SERVER['DOCUMENT_ROOT'] .'/config/Session.php';
require_once $_SERVER['DOCUMENT_ROOT'] .'/models/Product.php';

class Index extends Session {

  private $_product;

  public function __construct()
  {
    parent::__construct();
    $this->_product = new Product();
  }

  /**
   * Permet d'obtenir les nouveautés
   * @method getNews
   * @return array [description]
   */
  public function getNews() : array
  {
    return $this->_product->findNews();
  }

}

$_index = new Index();
$productsNews = $_index->getNews();

// Intégration du HTML
ob_start();
include $_SERVER['DOCUMENT_ROOT'] . '/views/home.phtml';
$template = ob_get_clean();
include $_SERVER['DOCUMENT_ROOT'] . '/views/layout.phtml';
