<?php

error_reporting(E_ALL);
ini_set('display_errors', 1);

require_once $_SERVER['DOCUMENT_ROOT'] .'/models/User.php';
require_once $_SERVER['DOCUMENT_ROOT'] .'/utilities/Checker.php';

class RegisterCtrl {

  private $_user;
  private $_validator;
  public $error;

  public function __construct()
  {
    $this->_user = new User();
    $this->_validator = new Checker();
    $this->error = null;
    if($_POST){
      $this->redirection();
    }
  }

  public function getEmail()
  {
    if(isset($_POST['email']) && !empty($_POST['email'])){
      $email = $_POST['email'];
      $check = $this->_validator->checkEmailFormat($email);

      if(!$check){
        $this->error = 'Email / Mot de passe incorrect';
        return false;
      }

      $result = $this->_user->findUserByEmail($email);
      if ($result) {
        $this->error = 'Email possédant déjà un compte';
        return false;
      }
      return $email;
    }
  }

  public function getPassword()
  {
    if(isset($_POST['password']) && !empty($_POST['password'])){
      $password = $_POST['password'];
      $check = $this->_validator->checkPasswordFormat($password);

      if(!$check){
        $this->error = 'Email / Mot de passe incorrect';
        return false;
      }

      $passwordHash = password_hash($password, PASSWORD_DEFAULT);

      return $passwordHash;
    }
  }

  public function getFirstName()
  {
    if(isset($_POST['first_name']) && !empty($_POST['first_name'])){
      $firstName = $_POST['first_name'];
      $check = $this->_validator->checkWord($firstName);

      if(!$check){
        $this->error = 'Le prénom ne doit contenir que des lettres';
        return false;
      }

      return $firstName;
    }
  }

  public function getLastName()
  {
    if(isset($_POST['last_name']) && !empty($_POST['last_name'])){
      $lastName = $_POST['last_name'];
      $check = $this->_validator->checkWord($lastName);

      if(!$check){
        $this->error = 'Le nom ne doit contenir au moins 3 caractères';
        return false;
      }

      return $lastName;
    }
  }

  public function getAddress()
  {
    if(isset($_POST['address']) && !empty($_POST['address'])){
      $address = $_POST['address'];
      $check = $this->_validator->checkAddress($address);

      if(!$check){
        $this->error = "L'adresse ne doit contenir que des lettres et des nombres";
        return false;
      }

      return $address;
    }
  }


  public function getPostalCode()
  {
    if(isset($_POST['postal_code']) && !empty($_POST['postal_code'])){
      $postalCode = intval($_POST['postal_code']);
      $check = $this->_validator->checkPostalCode($postalCode);

      if(!$check){
        $this->error = "Le code postal doit contenir 5 chiffres";
        return false;
      }

      return $postalCode;
    }
  }

  public function getCity()
  {
    if(isset($_POST['city']) && !empty($_POST['city'])){
      $city = $_POST['city'];
      $check = $this->_validator->checkWord($city);

      if(!$check){
        $this->error = 'Le nom ne doit contenir au moins 3 caractères';
        return false;
      }

      return $city;
    }
  }

  private function addUser()
  {
    $email = $this->getEmail();
    $passwordHash = $this->getPassword();
    $firstName = $this->getFirstName();
    $lastName = $this->getLastName();
    $address = $this->getAddress();
    $postalCode = $this->getPostalCode();
    $city = $this->getCity();

    if($email && $passwordHash && $firstName && $lastName && $address && $postalCode && $city){
      // Insertion du nouvel utilisateur dans la db
      $datas = [
        'email'       => $email,
        'password'    => $passwordHash,
        'status'      => 3,
        'address'     => $address,
        'city'        => $city,
        'postal_code' => $postalCode,
        'created_at'  => date('Y-m-d H:i:s'),
        'first_name'  => $firstName,
        'last_name'   => $lastName
      ];

      $userId = $this->_user->addAccount($datas);

      return intval($userId);

    } else {
      return false;
    }

  }

  public function redirection()
  {
    $userId = $this->addUser();

    // Redirection vers son espace utilisateur
    if(is_numeric($userId)){
      session_start();
      $_SESSION['id'] = $userId;
      $_SESSION['status'] = 3;
      header('Location: user/AccountCtrl.php');
    } else {
      return $this->error;
    }
  }

}

$_register = new RegisterCtrl();

ob_start();
include $_SERVER['DOCUMENT_ROOT'] . '/views/register.phtml';
$template = ob_get_clean();
include $_SERVER['DOCUMENT_ROOT'] . '/views/layout.phtml';
