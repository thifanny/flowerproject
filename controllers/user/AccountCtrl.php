<?php

error_reporting(E_ALL);
ini_set('display_errors', 1);

require_once $_SERVER['DOCUMENT_ROOT'] .'/config/Session.php';
require_once $_SERVER['DOCUMENT_ROOT'] .'/utilities/Checker.php';
require_once $_SERVER['DOCUMENT_ROOT'] .'/models/User.php';

class AccountCtrl extends Session {

  protected $userId;
  protected $userStatus;
  private $_user;
  public $error;

  public function __construct()
  {
    parent::__construct();

    $this->_user = new User();

    if ( isset($_POST['first_name'])  && !empty($_POST['first_name'])
    && isset($_POST['last_name'])   && !empty($_POST['last_name'])
    && isset($_POST['email'])       && !empty($_POST['email'])
    && isset($_POST['address'])     && !empty($_POST['address'])
    && isset($_POST['city'])        && !empty($_POST['city'])
    && isset($_POST['postal_code']) && !empty($_POST['postal_code']))
    {
      $this->setUpdateInfo();
    }

    if(  isset($_POST['oldPwd']) && !empty($_POST['oldPwd'])
    && isset($_POST['newPwd']) && !empty($_POST['newPwd']))
    {
      $this->setUpdatePwd();
    }

    if(isset($_POST['delete']) && $_POST['delete'] == true){
      $this->delete();
    }
  }

  /**
   * Récupère le statut de connexion
   * @method getStatusUser
   * @return [type] [description]
   */
  public function getStatusUser(){
    return $this->userStatus;
  }

  /**
   * Récupère les informations de l'utilisateur
   * @method getInfoUser
   * @return $result
   */
  public function getInfoUser()
  {
    $result = $this->_user->findUserById($this->userId);
    return $result;
  }

  /**
   * Met à jour les informations de l'utilisateur
   * @method setUpdateInfo
   */
  private function setUpdateInfo()
  {
    $firstName = $_POST['first_name'];
    $lastName = $_POST['last_name'];
    $email = $_POST['email'];
    $address = $_POST['address'];
    $city = $_POST['city'];
    $postal_code = $_POST['postal_code'];

    $_validator = new Checker();

    $checkFirstName = $_validator->checkWord($firstName);
    if(!$checkFirstName){
      $this->error = 'Prénom incorrect';
      return false;
    }

    $checkLastName = $_validator->checkWord($lastName);
    if(!$checkLastName){
      $this->error = 'Nom incorrect';
      return false;
    }

    $checkEmail = $_validator->checkEmailFormat($email);
    if(!$checkEmail){
      $this->error = 'Email incorrect';
      return false;
    }

    $checkAddress = $_validator->checkAddress($address);
    if(!$checkAddress){
      $this->error = 'Adresse incorrecte';
      return false;
    }

    $checkCity = $_validator->checkWord($city);
    if(!$checkCity){
      $this->error = 'Ville incorrecte';
      return false;
    }

    $checkPostalCode = $_validator->checkPostalCode($postal_code);
    if(!$checkPostalCode){
      $this->error = 'Code postal incorrect';
      return false;
    }

    $datas = [
      'id'          => $this->userId,
      'firstName'   => $firstName,
      'lastName'    => $lastName,
      'email'       => $email,
      'address'     => $address,
      'city'        => $city,
      'postal_code' => $postal_code
    ];

    $this->_user->updateInfo($datas);
  }

  /**
   * Met à jour le mot de passe
   * @method setPwd
   */
  public function setUpdatePwd()
  {
    $oldPwd = $_POST['oldPwd'];
    $newPwd = $_POST['newPwd'];

    $user = $this->getInfoUser();
    $test = password_verify($oldPwd, $user['password']);

    if($test){
      $_validator = new Checker();
      $check = $_validator->checkPasswordFormat($newPwd);

      if(!$check){
        $this->error = 'Email / Mot de passe incorrect';
        return false;
      }

      $passwordHash = password_hash($newPwd, PASSWORD_DEFAULT);

      $datas = [
        'password'  => $passwordHash,
        'id'        => $user['id']
      ];

      $result = $this->_user->updatePwd($datas);
      return $result;

    } else {
      $this->error = "L'ancien mot de passe n'est pas juste";
      return false;
    }

  }

  public function delete()
  {
    $user = $this->getInfoUser();
    $result = $this->_user->delete($user['id']);

    if($result){
      parent::destroy();
      header('Location: ../../index.php');
    }

    return $result;

  }

}

$_account = new AccountCtrl();

if($_account->getStatusUser() == 3){

  if($_account->getInfoUser()){
    $user = $_account->getInfoUser();
  }

  ob_start();
  include $_SERVER['DOCUMENT_ROOT'] . '/views/user/account.phtml';
  $template = ob_get_clean();
  include $_SERVER['DOCUMENT_ROOT'] . '/views/layout.phtml';

} else {
  ob_start();
  include $_SERVER['DOCUMENT_ROOT'] . '/views/error.phtml';
  $template = ob_get_clean();
  include $_SERVER['DOCUMENT_ROOT'] . '/views/layout.phtml';
}
