<?php

error_reporting(E_ALL);
ini_set('display_errors', 1);

require_once $_SERVER['DOCUMENT_ROOT'] .'/config/Session.php';
require_once $_SERVER['DOCUMENT_ROOT'] .'/models/Product.php';
require_once $_SERVER['DOCUMENT_ROOT'] .'/models/Order.php';

class PostOrderCtrl extends Session {

  protected $userId;
  protected $userStatus;
  private $_order;
  private $_product;

  public function __construct()
  {
    parent::__construct();

    if(isset($_POST['cart']) && !empty($_POST['cart'])){
      $this->_order = new Order();
      $this->_product = new Product();
      // $this->setBillDetails();
    }
  }

  /**
   * Récupère le statut de connexion
   * @method getStatusUser
   * @return [type] [description]
   */
  public function getStatusUser(){
    return $this->userStatus;
  }

  public function setBill()
  {

    $date = new DateTime();
    $dateNow = $date->format('Y-m-d H:i:s');

    $datasBill = [
      'user_id'     => $this->userId,
      'created_at'  => $dateNow,
      'total'       => null,
      'status'      => 3
    ];

    return $idBill = $this->_order->addBill($datasBill);
  }

  public function setBillDetails()
  {
    $idBill = $this->setBill();

    $productCart = [];

    $cart = $_POST['cart'];
    $posts = json_decode($cart);

    $count = array_count_values($posts);
    $total = 0;

    foreach($count as $idProduct => $elt){
      $product = $this->_product->findOne($idProduct);

      $product['qty'] = $elt;
      $product['totalArticle'] = $product['qty'] * $product['price'];
      $total += $product['totalArticle'];

      $datasBillDetails = [
        'bill_id'     => $idBill,
        'product_id'  => $idProduct,
        'product_qty' => $product['qty'],
        'subtotal'    => $product['totalArticle']
      ];

      $idBillDetails = $this->_order->addBillDetails($datasBillDetails);

    }

    $datasBillTotal = [
      'total' => $total,
      'id'    => $idBill
    ];

    $result = $this->_order->updateTotal($datasBillTotal);

    return $result;

  }

}

$_postOrder = new PostOrderCtrl();

if($_postOrder->getStatusUser() == 3){

  // Intégration du HTML
  ob_start();
  include $_SERVER['DOCUMENT_ROOT'] . '/views/user/postOrder.phtml';
  $template = ob_get_clean();
  include $_SERVER['DOCUMENT_ROOT'] . '/views/layout.phtml';

} else {
  ob_start();
  include $_SERVER['DOCUMENT_ROOT'] . '/views/error.phtml';
  $template = ob_get_clean();
  include $_SERVER['DOCUMENT_ROOT'] . '/views/layout.phtml';
}
