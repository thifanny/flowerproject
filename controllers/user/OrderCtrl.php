<?php

error_reporting(E_ALL);
ini_set('display_errors', 1);

require_once $_SERVER['DOCUMENT_ROOT'] .'/config/Session.php';
require_once $_SERVER['DOCUMENT_ROOT'] .'/models/Order.php';

class OrderCtrl extends Session {

  protected $userId;
  protected $userStatus;
  private $order;

  private $orders;
  private $orderDetails;

  public function __construct()
  {
    parent::__construct();
    $this->order = new Order();
  }

  /**
   * Récupère le statut de connexion
   * @method getStatusUser
   * @return [type] [description]
   */
  public function getStatusUser(){
    return $this->userStatus;
  }

  /**
  * Applique un format 'd/m/Y' à une date
  * @method setFormatDate
  * @param  string $strDate
  * @return $date
  */
  private function setFormatDate($strDate)
  {
    $date = new DateTime($strDate);
    return $date->format('d/m/Y');
  }

  /**
   * Transformation la date (chaine de caractère en objet DateTime() )
   * @method setDeliveryDate
   * @param string $orderDate
   * @return $deliveryDate
   */
  private function setDeliveryDate($orderDate)
  {
    $date = new DateTime($orderDate);
    $deliveryDate = $date->add(new DateInterval('P07D'));
    return $deliveryDate->format('d/m/Y');
  }

  /**
   * Donne le statut selon sa référence numérique
   * @method setStatus
   * @param integer $status
   * @return string
   */
  private function setStatus($status)
  {
    switch($status){
      case 1 :
        return 'Envoyé';
        break;
      case 2 :
        return 'En cours de préparation';
        break;
      case 3 :
        return 'En attente';
        break;
      default :
        return 'Statut inconnu';
        break;
    }
  }

  /**
   * Récupère les commandes de l'utilisateur connecté
   * @method getOrders
   * @return array $orders
   */
  public function getOrders()
  {
    $results = $this->order->findByUser($this->userId);

    $this->orders = [];

    foreach ($results as $result){
      array_push($this->orders, [
        'id'            => $result['id'],
        'orderDate'     => $this->setFormatDate($result['created_at']),
        'deliveryDate'  => $this->setDeliveryDate($result['created_at']),
        'total'         => $result['total'],
        'status'        => $this->setStatus($result['status'])
      ]);
    }

    return $this->orders;
  }

  /**
   * Récupère les détails d'une commande
   * @method getOrderDetails
   * @param  integer $idBill
   * @return array $orderDetails
   */
  public function getOrderDetails($idBill)
  {
    $results = $this->order->findBillDetails($idBill);

    $this->orderDetails = [];

    foreach ($results as $result){
      array_push($this->orderDetails, [
        'productName'   => $result['name'],
        'productQty'    => $result['product_qty'],
        'subtotal'      => $result['subtotal']
      ]);
    }

    return $this->orderDetails;
  }

}

//Instanciation du controleur
$_order = new OrderCtrl();

if($_order->getStatusUser() == 3){

  $orders = $_order->getOrders();

  // Intégration du HTML
  ob_start();
  include $_SERVER['DOCUMENT_ROOT'] . '/views/user/order.phtml';
  $template = ob_get_clean();
  include $_SERVER['DOCUMENT_ROOT'] . '/views/layout.phtml';

} else {
  ob_start();
  include $_SERVER['DOCUMENT_ROOT'] . '/views/error.phtml';
  $template = ob_get_clean();
  include $_SERVER['DOCUMENT_ROOT'] . '/views/layout.phtml';
}
