<?php

error_reporting(E_ALL);
ini_set('display_errors', 1);

require_once $_SERVER['DOCUMENT_ROOT'] .'/config/Session.php';
require_once $_SERVER['DOCUMENT_ROOT'] .'/models/Product.php';

class AddCtrl extends Session {

  protected $userStatus;
  private $_product;
  public $error;

  public function __construct()
  {
    parent::__construct();

    if(!empty($_POST)){
      $this->_product = new Product();
      $this->redirection();
    }

  }

  /**
   * Récupère le statut de connexion
   * @method getStatusUser
   * @return [type] [description]
   */
  public function getStatusUser(){
    return $this->userStatus;
  }

  /**
   * Traitement de l'image
   * @method getImg
   */
  private function getImg()
  {

    if(isset($_FILES['img'])){

      $repertory = $_SERVER['DOCUMENT_ROOT'] . '/views/public/img/flower/';
      $file = basename($_FILES['img']['name']);

      $maxSize = 100000;
      $size = filesize($_FILES['img']['tmp_name']);
      $exts = array('.png', '.gif', '.jpg', '.jpeg');
      $ext = strrchr($_FILES['img']['name'], '.');

      if(!in_array($ext, $exts)) {
        $this->error = 'Vous devez uploader un fichier de type png, gif, jpg, jpeg';
        return false;
      }

      if($size > $maxSize) {
        $this->error = 'Le fichier est trop gros';
        return false;
      }

     //On formate le nom du fichier
     $file = strtr($file,
      'ÀÁÂÃÄÅÇÈÉÊËÌÍÎÏÒÓÔÕÖÙÚÛÜÝàáâãäåçèéêëìíîïðòóôõöùúûüýÿ',
      'AAAAAACEEEEIIIIOOOOOUUUUYaaaaaaceeeeiiiioooooouuuuyy');
     $file = preg_replace('/([^.a-z0-9]+)/i', '-', $file);

     if(move_uploaded_file($_FILES['img']['tmp_name'], $repertory . $file)){
      return $file;
     }
     else
     {
       return false;
     }

    }

  }

  /**
   * Ajout du produit à la db
   * @method addProduct
   */
  private function addProduct()
  {

    $path_img = $this->getImg();

    if($path_img){

      if(isset($_POST['name']) && isset($_POST['reference']) && isset($_POST['price']) && isset($_POST['quantity'])
      && isset($_POST['description'])){

        $name = $_POST['name'];
        $reference = $_POST['reference'];
        $price = $_POST['price'];
        $quantity = $_POST['quantity'];
        $path_img = $path_img;
        $description = $_POST['description'];

        $datas = [
          'name'        => $name,
          'reference'   => $reference,
          'price'       => $price,
          'quantity'    => $quantity,
          'path_img'    => $path_img,
          'description' => $description,
          'created_at'  => date('Y-m-d H:i:s')
        ];

        $result = $this->_product->add($datas);

        return $result;

      } else {

        return false;

      }

    } else {

      return false;

    }

  }

  /**
   * Redirection si l'ajout s'est correctement déroulé
   * @method redirection
   * @return [type] [description]
   */
  public function redirection()
  {
    $result = $this->addProduct();

    if($result){
      header('Location: ListProductsCtrl.php');
    } else {
      return $this->error;
    }
  }

}

$_add = new AddCtrl();

if($_add->getStatusUser() == 1){

  ob_start();
  include $_SERVER['DOCUMENT_ROOT'] . '/views/admin/product/add.phtml';
  $template = ob_get_clean();
  include $_SERVER['DOCUMENT_ROOT'] . '/views/layout.phtml';

} else {
  ob_start();
  include $_SERVER['DOCUMENT_ROOT'] . '/views/error.phtml';
  $template = ob_get_clean();
  include $_SERVER['DOCUMENT_ROOT'] . '/views/layout.phtml';
}
