<?php

error_reporting(E_ALL);
ini_set('display_errors', 1);

require_once $_SERVER['DOCUMENT_ROOT'] .'/config/Session.php';
require_once $_SERVER['DOCUMENT_ROOT'] .'/models/Product.php';

class DeleteCtrl extends Session {

  private $_product;
  protected $userStatus;

  public function __construct()
  {
    parent::__construct();
    $this->_product = new Product();

    if(isset($_GET['id'])){
      $id = intval($_GET['id']);
      $this->delete($id);
    }

  }

  /**
   * Récupère le statut de connexion
   * @method getStatusUser
   * @return [type] [description]
   */
  public function getStatusUser(){
    return $this->userStatus;
  }

  /**
   * Suppression de l'existence d'un article sur le site
   * @method delete
   * @param  int $id [id du produit]
   * @return bool $result
   */
  public function delete($id) : bool
  {
    $result = $this->_product->displayOff($id);
    return $result;
  }

}

$_delete = new DeleteCtrl();

if($_delete->getStatusUser() == 1){

  ob_start();
  include $_SERVER['DOCUMENT_ROOT'] . '/views/admin/product/delete.phtml';
  $template = ob_get_clean();
  include $_SERVER['DOCUMENT_ROOT'] . '/views/layout.phtml';

} else {
  ob_start();
  include $_SERVER['DOCUMENT_ROOT'] . '/views/error.phtml';
  $template = ob_get_clean();
  include $_SERVER['DOCUMENT_ROOT'] . '/views/layout.phtml';
}
