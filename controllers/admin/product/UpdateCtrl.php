<?php

error_reporting(E_ALL);
ini_set('display_errors', 1);

require $_SERVER['DOCUMENT_ROOT'] .'/config/Session.php';
require $_SERVER['DOCUMENT_ROOT'] .'/models/Product.php';

class UpdateCtrl extends Session
{

  private $_product;
  public $error;
  protected $userStatus;

  public function __construct()
  {
    parent::__construct();
    $this->_product = new Product();

    if (!empty($_POST)) {
      $this->update();
    }
  }

  /**
   * Récupère le statut de connexion
   * @method getStatusUser
   * @return [type] [description]
   */
  public function getStatusUser(){
    return $this->userStatus;
  }

  /**
   * Traitement de l'image
   * @method getImg
   */
  private function getImg()
  {

    if(isset($_FILES['img'])){

      $repertory = $_SERVER['DOCUMENT_ROOT'] . '/views/public/img/flower/';
      $file = basename($_FILES['img']['name']);

      $maxSize = 100000;
      $size = filesize($_FILES['img']['tmp_name']);
      $exts = array('.png', '.gif', '.jpg', '.jpeg');
      $ext = strrchr($_FILES['img']['name'], '.');

      if(!in_array($ext, $exts)) {
        $this->error = 'Vous devez uploader un fichier de type png, gif, jpg, jpeg';
        return false;
      }

      if($size > $maxSize) {
        $this->error = 'Le fichier est trop gros';
        return false;
      }

     //On formate le nom du fichier
     $file = strtr($file,
      'ÀÁÂÃÄÅÇÈÉÊËÌÍÎÏÒÓÔÕÖÙÚÛÜÝàáâãäåçèéêëìíîïðòóôõöùúûüýÿ',
      'AAAAAACEEEEIIIIOOOOOUUUUYaaaaaaceeeeiiiioooooouuuuyy');
     $file = preg_replace('/([^.a-z0-9]+)/i', '-', $file);

     if(move_uploaded_file($_FILES['img']['tmp_name'], $repertory . $file)){
      return $file;
     }
     else
     {
       return false;
     }

    }

  }

  private function update()
  {

    $path_img = $this->getImg();

    if($path_img){
      if(isset($_POST['id']) && isset($_POST['name']) && isset($_POST['reference']) && isset($_POST['price']) && isset($_POST['quantity'])
      && isset($_POST['description'])){

        $id = $_POST['id'];
        $name = $_POST['name'];
        $reference = $_POST['reference'];
        $price = $_POST['price'];
        $quantity = $_POST['quantity'];
        $description = $_POST['description'];

        $datas = [
          'id'          => $id,
          'name'        => $name,
          'reference'   => $reference,
          'price'       => $price,
          'quantity'    => $quantity,
          'path_img'    => $path_img,
          'description' => $description
        ];

        $result = $this->_product->update($datas);

        return $result;

      } else {
        return false;
      }
    } else {
      return false;
    }

  }

  public function getOne($id)
  {
    return $this->_product->findOne($id);
  }

  public function getError(){
    return $this->error;
  }

  public function redirection()
  {
    $result = $this->update();

    if(!$result){
      return $this->error;
    }
  }

}

$_update = new UpdateCtrl();

if($_update->getStatusUser() == 1){


  if(isset($_GET['id'])){
    ob_start();
    $id = $_GET['id'];
    $product = $_update->getOne($id);
    include $_SERVER['DOCUMENT_ROOT'] . '/views/admin/product/update.phtml';
    $template = ob_get_clean();
    include $_SERVER['DOCUMENT_ROOT'] . '/views/layout.phtml';
  }

} else {
  ob_start();
  include $_SERVER['DOCUMENT_ROOT'] . '/views/error.phtml';
  $template = ob_get_clean();
  include $_SERVER['DOCUMENT_ROOT'] . '/views/layout.phtml';
}
