<?php

error_reporting(E_ALL);
ini_set('display_errors', 1);

require_once $_SERVER['DOCUMENT_ROOT'] .'/config/Session.php';
require_once $_SERVER['DOCUMENT_ROOT'] .'/models/Product.php';

class ListProductsCtrl extends Session {

  private $_product;
  protected $userStatus;

  public function __construct()
  {
    parent::__construct();
    $this->_product = new Product();
  }

  /**
   * Récupère le statut de connexion
   * @method getStatusUser
   * @return [type] [description]
   */
  public function getStatusUser(){
    return $this->userStatus;
  }

  /**
   * Récupère tous les produits de la DB
   * @method getProducts
   * @return array
   */
  public function getProducts() : array
  {
    return $this->_product->findAll();
  }

  /**
   * Récupère un produit de la DB par son $id
   * @method getProduct
   * @param  int $id
   * @return array
   */
  public function getProduct(int $id) : array
  {
    return $this->_product->findOne($id);
  }

}


$_listProducts = new ListProductsCtrl();

if($_listProducts->getStatusUser() == 1){

  $products = $_listProducts->getProducts();

  ob_start();
  if(isset($_GET['id'])){
    $id = intval($_GET['id']);
    $productOne = $_listProducts->getProduct($id);
    include $_SERVER['DOCUMENT_ROOT'] . '/views/admin/product/update.phtml';
  } else {
    include $_SERVER['DOCUMENT_ROOT'] . '/views/admin/product/listProducts.phtml';
  }
  $template = ob_get_clean();
  include $_SERVER['DOCUMENT_ROOT'] . '/views/layout.phtml';

} else {
  ob_start();
  include $_SERVER['DOCUMENT_ROOT'] . '/views/error.phtml';
  $template = ob_get_clean();
  include $_SERVER['DOCUMENT_ROOT'] . '/views/layout.phtml';
}
