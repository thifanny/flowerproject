<?php

error_reporting(E_ALL);
ini_set('display_errors', 1);

require_once $_SERVER['DOCUMENT_ROOT'] .'/config/Session.php';
require_once $_SERVER['DOCUMENT_ROOT'] .'/models/User.php';

class ListCtrl extends Session {

  protected $userId;
  protected $userStatus;
  private $_user;

  public function __construct()
  {
    parent::__construct();
    $this->_user = new User();
  }
  
  /**
   * Récupère le statut de connexion
   * @method getStatusUser
   * @return [type] [description]
   */
  public function getStatusUser(){
    return $this->userStatus;
  }

  public function getUsers()
  {
    return $this->_user->findAll();
  }

}


$_list = new ListCtrl();

if($_list->getStatusUser() == 1){
  $users = $_list->getUsers();

  ob_start();
  include $_SERVER['DOCUMENT_ROOT'] . '/views/admin/customer/list.phtml';
  $template = ob_get_clean();
  include $_SERVER['DOCUMENT_ROOT'] . '/views/layout.phtml';

} else {
  ob_start();
  include $_SERVER['DOCUMENT_ROOT'] . '/views/error.phtml';
  $template = ob_get_clean();
  include $_SERVER['DOCUMENT_ROOT'] . '/views/layout.phtml';
}
