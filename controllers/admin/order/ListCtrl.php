<?php

error_reporting(E_ALL);
ini_set('display_errors', 1);

require $_SERVER['DOCUMENT_ROOT'] .'/config/Session.php';
require $_SERVER['DOCUMENT_ROOT'] .'/utilities/Format.php';
require $_SERVER['DOCUMENT_ROOT'] .'/models/Order.php';
require $_SERVER['DOCUMENT_ROOT'] .'/models/User.php';

class ListCtrl extends Session
{
  protected $userStatus;
  private $_order;
  private $_user;

  public function __construct()
  {
    parent::__construct();
    $this->_user = new User();
    $this->_order = new Order();

    if(isset($_POST['status']) && isset($_POST['id'])
    && !empty($_POST['status']) && !empty($_POST['id'])){

      $status = intval($_POST['status']);
      $orderId = intval($_POST['id']);
      $datas = [
        'status'  => $status,
        'id'      => $orderId
      ];

      $this->updateStatus($datas);
    }

  }

  /**
   * Récupère le statut de connexion
   * @method getStatusUser
   * @return [type] [description]
   */
  public function getStatusUser(){
    return $this->userStatus;
  }

  /**
   * Récupère toutes les commandes
   * @method getOrders
   * @return array
   */
  public function getOrders() : array
  {
    $_format = new Format();

    $results = $this->_order->findAll();
    $orders = [];

    foreach ($results as $result){
      array_push($orders, [
        'user_id'       => $result['user_id'],
        'id'            => $result['id'],
        'orderDate'     => $_format->setFormatDate($result['created_at']),
        'total'         => $result['total'],
        'status'        => $result['status']
      ]);
    }

    return $orders;
  }

  public function getOrdersDetails($idBill) : array
  {
    return $this->_order->findBillDetails($idBill);
  }

  public function getUser($idUser)
  {
    return $this->_user->findUserById($idUser);
  }

  /**
  * [updateStatus description]
  * @method updateStatus
  * @param  array $datas [status, orderId]
  * @return bool
  */
  public function updateStatus($datas)
  {
    return $this->_order->updateStatus($datas);
  }

}

$_list = new ListCtrl();

if($_list->getStatusUser() == 1){
  $orders = $_list->getOrders();

  // Intégration du HTML
  ob_start();
  include $_SERVER['DOCUMENT_ROOT'] . '/views/admin/order/list.phtml';
  $template = ob_get_clean();
  include $_SERVER['DOCUMENT_ROOT'] . '/views/layout.phtml';
} else {
  ob_start();
  include $_SERVER['DOCUMENT_ROOT'] . '/views/error.phtml';
  $template = ob_get_clean();
  include $_SERVER['DOCUMENT_ROOT'] . '/views/layout.phtml';
}
