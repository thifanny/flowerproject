<?php

error_reporting(E_ALL);
ini_set('display_errors', 1);

require $_SERVER['DOCUMENT_ROOT'] .'/models/Product.php';

$product = new Product();
$products = $product->findAll();

ob_start();
if(isset($_GET['id'])){
  $id = $_GET['id'];
  $productOne = $product->findOne($id);
  include '../views/product.phtml';
} else {
  include '../views/products.phtml';
}
$template = ob_get_clean();
include '../views/layout.phtml';
