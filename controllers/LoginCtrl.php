<?php

error_reporting(E_ALL);
ini_set('display_errors', 1);

require $_SERVER['DOCUMENT_ROOT'] .'/config/Session.php';
require $_SERVER['DOCUMENT_ROOT'] .'/utilities/Checker.php';
require $_SERVER['DOCUMENT_ROOT'] .'/models/User.php';

class LoginCtrl {

  public $error;
  private $_validator;
  private $user;

  public function __construct()
  {
    $this->error = null;
    $this->_validator = new Checker;
    if($_POST){
      $this->redirection();
    }
  }

  /**
   * Vérification du format de l'email et du mot de passe
   * @method checkFormat
   * @param  string $email
   * @param  string $password
   * @return bool
   */
  public function checkFormat(string $email, string $password) : bool
  {
    $resultEmail = $this->_validator->checkEmailFormat($email);
    $resultPassword = $this->_validator->checkPasswordFormat($password);

    if ($resultEmail && $resultPassword){
      return true ;
    } else {
      return false;
    }
  }

  /**
   * Vérifie que l'utilisateur existe en bd et que le mdp est correct
   * @method checkUser
   * @param  string $email
   * @param  string $password
   * @return bool
   */
  public function checkUser(string $email, string $password) : bool
  {
    $checkFormat = $this->checkFormat($email, $password);

    if($checkFormat){
      $_user = new User();
      $this->user = $_user->findUserByEmail($email);

      $hash = $this->user['password'];
      $test = password_verify($password, $hash);

      if($this->user && $test){
        return true;
      } else {
        return false;
      }
    }

  }

  /**
   * Ammorce la session et redirige si les vérifications sont bonnes
   * @method redirection
   * @return [type] [description]
   */
  public function redirection()
  {
    $email = $_POST['email'];
    $password = $_POST['password'];

    $checkUser = $this->checkUser($email, $password);

    if($checkUser){

      session_start();
      $_SESSION['id'] = $this->user['id'];
      $_SESSION['status'] = intval($this->user['status']);

      if ($this->user['status'] == 3) {
        header('Location: user/AccountCtrl.php');
      } else {
        header('Location: admin/order/ListCtrl.php');
      }

    }

  }

}

$_loginCtrl = new LoginCtrl();
$error = $_loginCtrl->error;

ob_start();
include '../views/login.phtml';
$template = ob_get_clean();
include '../views/layout.phtml';
