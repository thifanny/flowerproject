<?php

error_reporting(E_ALL);
ini_set('display_errors', 1);

require $_SERVER['DOCUMENT_ROOT'] .'/config/Session.php';
require $_SERVER['DOCUMENT_ROOT'] .'/models/Product.php';
require $_SERVER['DOCUMENT_ROOT'] .'/models/User.php';

class CartCtrl extends Session {

  protected $userId;
  private $_product;
  private $_user;
  private $productCart;

  public function __construct()
  {
    parent::__construct();
    $this->_user = new User();
    $this->_product = new Product();
  }

  private function setCart($cart)
  {
    $productCart = [];
    $posts = json_decode($cart);

    if($posts){
      $count = array_count_values($posts);
      $total = 0;

      foreach($count as $id => $elt){
        $product = $this->_product->findOne($id);
        $product['qty'] = $elt;
        $product['totalArticle'] = $product['qty'] * $product['price'];
        $total += $product['totalArticle'];
        array_push($productCart, $product);
      }

      return [$productCart, $total];
    }
  }

  public function getCart()
  {

    if(isset($_POST['cart']) && !empty($_POST['cart'])){
      $cart = $_POST['cart'];
    }

    return $this->productCart = $this->setCart($cart);
  }

  public function getUser()
  {
    return $this->_user->findUserById($this->userId);
  }

}

$_cart = new CartCtrl();
$cart = $_cart->getCart();

$productCart = $cart[0];
$productTotal = $cart[1];

$user = $_cart->getUser();

// Intégration du HTML
ob_start();
include $_SERVER['DOCUMENT_ROOT'] . '/views/cart.phtml';
$template = ob_get_clean();
include $_SERVER['DOCUMENT_ROOT'] . '/views/layout.phtml';
