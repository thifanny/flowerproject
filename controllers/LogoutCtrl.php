<?php

error_reporting(E_ALL);
ini_set('display_errors', 1);

require_once $_SERVER['DOCUMENT_ROOT'] .'/config/Session.php';
require_once $_SERVER['DOCUMENT_ROOT'] .'/models/Product.php';
require_once $_SERVER['DOCUMENT_ROOT'] .'/models/Order.php';

class LogoutCtrl extends Session {


    public function __construct()
    {
      parent::__construct();
      parent::destroy();
      header('Location: /index.php');
    }


}

$_logout = new LogoutCtrl();
