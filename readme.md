# Projet : Savonne_fleuriste

Auteur : Thifanny Brochard

## Description :
- Simple site de communication entre client(HTML, CSS, JS) / serveur (PHP) / base de données (MySQL).

- Deux interfaces clients : admin (propriétaire de la boutique) et utilisateurs.

- Système simple de connexion et authentification.

## Installation :

- Importer 'Savonne_flower.sql' dans une base de données MySQL.

- 'Config > config.json' : modifier le host, le port, le nom d'utilisateur et le mot de passe de la base de données (au besoin).

- Lancer le serveur PHP.

## Utilisation :

- Afin d'observer l'interface admin :
  - Email :         admin@admin.fr
  - Mot de passe :  Admin123. ( /!\ Ne pas oublier le point)


- Afin d'observer l'interface user :
  - Email :         user1@user.fr
  - Mot de passe :  User1234. ( /!\ Ne pas oublier le point)


- Possibilité de créer un compte et de se connecter avec le nouveau compte
